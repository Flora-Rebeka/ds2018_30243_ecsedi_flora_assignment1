package repo.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "flights")
public class Flight implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "number")
	private Integer number;
	
	@Column(name = "airplane_type")
	private String airplaneType;
	
	//@ManyToOne(fetch = FetchType.LAZY)
	@Column(name = "departure_city_id")
	private Integer departureCity;
	
	@Column(name = "daparture_date_hour")
	private Timestamp departureDateHour;
	
	//@ManyToOne(fetch = FetchType.LAZY)
	@Column(name = "arrival_city_id")
	private Integer arrivalCity;
	
	@Column(name = "arrival_date_hour")
	private Timestamp arrivalDateHour;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public Integer getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(Integer departupeCity) {
		this.departureCity = departupeCity;
	}

	public Integer getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(Integer arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Timestamp getDepartureDateHour() {
		return departureDateHour;
	}

	public void setDepartureDateHour(Timestamp departureDateHour) {
		this.departureDateHour = departureDateHour;
	}

	public Timestamp getArrivalDateHour() {
		return arrivalDateHour;
	}

	public void setArrivalDateHour(Timestamp arrivalDateHour) {
		this.arrivalDateHour = arrivalDateHour;
	}
}
