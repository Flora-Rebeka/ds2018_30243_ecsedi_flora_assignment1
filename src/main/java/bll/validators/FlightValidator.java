package bll.validators;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import repo.models.Flight;

public class FlightValidator{

	public Flight isValid(String number, String airplaneType, 
			String departureCityId, String departureDateHour,
			String arrivalCityId, String arrivalDateHour) {

		StringBuilder departDH = new StringBuilder(departureDateHour);
		departDH.setCharAt(10, ' ');
		StringBuilder arrivalDH = new StringBuilder(arrivalDateHour);
		arrivalDH.setCharAt(10, ' ');
		
		
		String dep = departDH.toString();
		String arr = arrivalDH.toString();
		
		dep += ":00";
		arr += ":00";
		Flight flight = new Flight();
		try{
			Integer flightNumber = Integer.parseInt(number);
			Integer departureId = Integer.parseInt(departureCityId);
			Integer arrivalId = Integer.parseInt(arrivalCityId);
			
			System.out.println(dep);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(dep, formatter);
			Timestamp departureTime = Timestamp.valueOf(dateTime);
			
			System.out.println(arr);
			DateTimeFormatter formatterr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTimer = LocalDateTime.parse(arr, formatterr);
			Timestamp arrivalTime = Timestamp.valueOf(dateTimer);
			
			System.out.println("here 3");
			System.out.println(arrivalTime + " " + departureTime);
			//if(airplaneType.equals("transport")){// || airplaneType.equals("")){
				flight.setAirplaneType(airplaneType);
				flight.setArrivalCity(arrivalId);
				flight.setDepartureCity(departureId);
				flight.setArrivalDateHour(arrivalTime);
				flight.setDepartureDateHour(departureTime);
				flight.setNumber(flightNumber);
				return flight;
			//}
			//return null;
		}
		catch(Exception e){
			return null;
		}
	}

}
