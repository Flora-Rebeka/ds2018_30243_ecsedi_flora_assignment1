package bll.validators;

import java.util.List;

import bll.crypting.Hashing;
import repo.dal.UserDao;
import repo.models.User;

public class LoginValidator {

	public String isValid(String username, String password){
		
		List<User> users = UserDao.select();
		for(User user: users){
			if(user.getUsername().equals(username) && Hashing.verifyHash(password, user.getPassword())){
				return user.getType();
			}
		}
		return "invalid";
	}
}
