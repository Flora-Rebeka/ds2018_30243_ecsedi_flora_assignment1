package bll.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bll.services.AdminServices;
import bll.validators.FlightValidator;
import repo.models.Flight;

/**
 * Servlet implementation class FlightUpdateDelete
 */
@WebServlet("/FlightUpdateDelete")
public class FlightUpdateDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlightUpdateDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("flightId");
		Integer intId = Integer.parseInt(id);
		AdminServices as = new AdminServices();
		Flight flight = as.selectFlight(intId);
		if(flight != null){
			as.deleteFlight(flight);
			response.sendRedirect("AdminHome.html");
		}
		else{
			//some error msg
			response.sendRedirect("DeleteFlight.html");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String number = request.getParameter("flightNumber");
		String airplaneType = request.getParameter("flightType");
		String departureCityId = request.getParameter("departureId");
		String departureDateHour = request.getParameter("departureTime");
		String arrivalCityId = request.getParameter("arrivalId");
		String arrivalDateHour = request.getParameter("arrivalTime");
		System.out.println(departureDateHour);
		System.out.println(arrivalDateHour);
		AdminServices as = new AdminServices();
		FlightValidator fv = new FlightValidator();
		Flight flight = fv.isValid(number, airplaneType, 
				departureCityId, departureDateHour, 
				arrivalCityId,arrivalDateHour);
		
		if(flight != null){
			flight.setId(Integer.parseInt(request.getParameter("flightId")));
			as.updateFlight(flight);
			response.sendRedirect("AdminHome.html");
		}
		else{
			System.out.println("NULLLLLL");
			doGet(request, response);
		}
	}

}
