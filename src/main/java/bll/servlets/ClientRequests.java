package bll.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bll.services.AdminServices;
import bll.services.ClientServices;
import repo.models.Flight;

/**
 * Servlet implementation class ClientRequests
 */
@WebServlet("/ClientRequests")
public class ClientRequests extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientRequests() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		out.append("<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>"
				+ "View flight</title><link rel=\"stylesheet\" href=\"user.css\"></head>"
				+ "<div class=\"logout-btn\"><form action=\"Logout\" method=\"POST\">"
				+ "<input type=\"submit\" value=\"Logout\"/></form></div>"
				+ "<h1> WELCOME </h1><div class=\"admin-page left\"><div class=\"form\">"
				+ "<p>VIEW FLIGHTS</p><a href=\"/maven-web-project-example/ClientRequests\">VIEW</a><p>"
				+ "QUERY</p><a href=\"Query.html\">QUERY</a>"
				+ "</div></div>");
		
		out.append("<div class=\"admin-page right\"><div class=\"form\">");
		
		AdminServices as = new AdminServices();
		List<Flight> flights = as.findAllFlight();
		for(Flight f: flights){
			out.append("<p>" + as.getFlightString(f) + "</p>" + "\n");
		}
		out.append("</div></div>" + "</html>");
	}
}
