package bll.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bll.services.ClientServices;

/**
 * Servlet implementation class ClientResponse
 */
@WebServlet("/ClientResponse")
public class ClientResponse extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String time = "";
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String cityName = request.getParameter("city_name");
		ClientServices cs = new ClientServices();
		String localTime = cs.getLocalTimeByCityName(cityName);
		time = localTime;
		PrintWriter out = response.getWriter();
		
		out.append("<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>"
				+ "View flight</title><link rel=\"stylesheet\" href=\"user.css\"></head>"
				+ "<h1> WELCOME </h1><div class=\"admin-page left\"><div class=\"form\">"
				+ "<p>VIEW FLIGHTS</p><a href=\"/maven-web-project-example/ClientRequests\">VIEW</a><p>"
				+ "QUERY</p><a href=\"Query.html\">QUERY</a>"
				+ "</div></div>");
		out.append("<div class=\"admin-page right\"><div class=\"form\">"+ "<p>LOCAL TIME IS</p>"
				+ time+ "</div></div>" + "</html>");
	}
}
