package bll.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bll.services.AdminServices;
import bll.validators.FlightValidator;
import repo.models.Flight;

/**
 * Servlet implementation class FlightAction
 */
@WebServlet("/FlightAction")
public class FlightAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlightAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out = response.getWriter();
		System.out.println("GET");
		
		out.append("<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>"
				+ "View flight</title><link rel=\"stylesheet\" href=\"user.css\"></head>"
				+ "<div class=\"logout-btn\"><form action=\"Logout\" method=\"POST\">"
				+ "<input type=\"submit\" value=\"Logout\"/></form></div>"
				+ "<h1> WELCOME </h1><div class=\"admin-page left\"><div class=\"form\">"
				+ "<p>CREATE A NEW FLIGHT</p><a href=\"CreateFlight.html\">CREATE</a>"
				+ "<p>VIEW FLIGHTS</p><a href=\"/maven-web-project-example/FlightAction\">VIEW</a><p>"
				+ "UPDATE AN EXISTING FLIGHT</p><a href=\"UpdateFlight.html\">UPDATE</a>"
				+ "<p>DELETE AN EXISTING FLIGHT</p><a href=\"DeleteFlight.html\">DELETE</a>"
				+ "</div></div>");
		
		out.append("<div class=\"admin-page right\"><div class=\"form\">");
		
		AdminServices as = new AdminServices();
		List<Flight> flights = as.findAllFlight();
		for(Flight f: flights){
			out.append("<p>" + as.getFlightString(f) + "</p>" + "\n");
		}
		out.append("</div></div>" + "</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String number = request.getParameter("flightNumber");
		String airplaneType = request.getParameter("flightType");
		String departureCityId = request.getParameter("departureId");
		String departureDateHour = request.getParameter("departureTime");
		String arrivalCityId = request.getParameter("arrivalId");
		String arrivalDateHour = request.getParameter("arrivalTime");
		System.out.println(departureDateHour);
		System.out.println(arrivalDateHour);
		AdminServices as = new AdminServices();
		FlightValidator fv = new FlightValidator();
		Flight flight = fv.isValid(number, airplaneType, 
				departureCityId, departureDateHour, 
				arrivalCityId,arrivalDateHour);
		if(flight != null){
			as.insertFlight(flight);
			response.sendRedirect("AdminHome.html");
		}
		else{
			System.out.println("NULLLLLL");
			doGet(request, response);
		}
	}
}
