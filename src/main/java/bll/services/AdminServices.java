package bll.services;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import bll.validators.FlightValidator;
import repo.dal.CityDao;
import repo.dal.FlightDao;
import repo.models.City;
import repo.models.Flight;

public class AdminServices {

	public void insertFlight(Flight flight){
		FlightDao.insert(flight);
	}
	
	public void updateFlight(Flight flight){
		FlightDao.update(flight);
	}
	
	public void deleteFlight(Flight flight){
		FlightDao.delete(flight);
	}
	
	public List<Flight> findAllFlight(){
		return FlightDao.select();
	}
	
	public Flight selectFlight(Integer id){
		List<Flight> flights = this.findAllFlight();
		for(Flight f: flights){
			if((int)f.getId() == (int)id){
				return f;
			}
		}
		return null;
	}
	
	public City getCity(Integer id){
		List<City> cities = CityDao.select();
		for(City c: cities){
			if(c.getId().equals(id)){
				return c;
			}
		}
		return null;
	}
	
	public Calendar timestampToCalendar(Timestamp timestamp){
		Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp.getTime());
        return calendar;
	}
	
	public String getFlightString(Flight flight){
		String fly = "";
		
		fly += (flight.getId());
		fly += "-";
		fly += (flight.getNumber());
		fly += " ";
		fly += flight.getAirplaneType();
		fly += " ";
		fly += this.getCity(flight.getDepartureCity()).getName();
		fly += " ";
		fly += flight.getDepartureDateHour().toString();//this.timestampToCalendar(flight.getDepartureDateHour());
		fly += " ";
		fly += this.getCity(flight.getArrivalCity()).getName();
		fly += " ";
		fly += flight.getArrivalDateHour().toString();//this.timestampToCalendar(flight.getArrivalDateHour());
		return fly;
	}
}
