package bll.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import repo.dal.CityDao;
import repo.models.City;

public class ClientServices {

	public String getLocalTime(double longitude, double latitude){
		
		String toReturn = null;
		String url = "http://www.new.earthtools.org/timezone/";
		URL external;
		try {
			external = new URL(url + latitude + "/" + longitude);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					external.openStream()));
			String line;
			int counter = 0;
			while((line = in.readLine()) != null){
				counter++;
				if(counter == 10){
					System.out.println(line.substring(13,33));
					toReturn = line.substring(13,33);
				}
			}
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return toReturn;
	}
	
	public String getLocalTimeByCityName(String cityName){
		
		double longitude = 0, latitude = 0;
		List<City> cities = CityDao.select();
		for(City city: cities){
			if(city.getName().equals(cityName)){
				longitude = city.getLongitude();
				latitude = city.getLatitude();
			}
		}
		return this.getLocalTime(longitude, latitude);
	}
}
